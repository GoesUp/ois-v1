import java.lang.Math;
import java.util.Scanner;

public class Gravitacija {
    public static void main(String[] args) {
        System.out.println("OIS je zakon!");
        Scanner s = new Scanner(System.in);
        double visina = s.nextDouble();
        double gravPosp = izracunGravitacije(visina);
        izpis(gravPosp, visina);
    }
    
    public static double izracunGravitacije(double visina) {
        double c = 6.674 * Math.pow(10, -11);
        double m = 5.972 * Math.pow(10, 24);
        double r = 6.371 * Math.pow(10, 6);
        return (c * m) / Math.pow(r + visina, 2);
    }

    public static void izpis(double vrednost, double visina) {
        System.out.printf("Visina: %f, gravitacijski pospesek: %f.%n", visina, vrednost);
    }
}
